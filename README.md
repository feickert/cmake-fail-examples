# cmake-fail-examples

This example repo uses Git submodules so be sure to clone with

```
git clone --recursive ssh://git@gitlab.cern.ch:7999/feickert/cmake-fail-examples.git
```

The repo contains two directories that both show a failing build attempt with just using the external dependencies and `atlas_project`.
`CMake_FetchConent/` attempts to use the CMake `FetchContent_Declare` and `FetchContent_MakeAvailable` patterns, while `Git_Submodules` attempts to use Git submodules in the top level directory with the `CMakeLists.txt`.

## Build

To reproduce the build failure, `cd` into either directory somewhere with `CVMFS` access (for `setupATLAS`) and then just run

```
bash build.sh
```

which will result in an error.

Attila was able to show an example of building xAODAnaHelpers by appending its `xAODAnaHelpersLib.dsomap` into the global cache, but I'm not sure how that would work in this situation.
It was also made clear that using `FetchContent` is not the recommended manner, but it is then unclear how to use submodules if they also fail.

## Note on dependencies build without `atlas_project`

In **both** cases, if `atlas_project` is commented out in the `CMakeLists.txt`

```
# Set up our analysis project.
#atlas_project( build_example 1.0.0
#   USE AnalysisBase 21.2 )
```

then the external dependencies can be built without errors.
So it is unclear to me how to properly position these hard dependencies and still have ATLAS CMake work.

The build order is sensitive, as the actual project code in the real project (not included here to keep the example minimal) needs to be able to import `frugally-deep`, so `frugally-deep` and all its dependencies need to be available before that code is compiled.
